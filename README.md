# educacaovigiada-org-br

## Tecnologias

![Javascript badge](https://img.shields.io/badge/javascript%20-%23323330.svg?&style=for-the-badge&logo=javascript&logoColor=%23F7DF1E)
![CSS3 badge](https://img.shields.io/badge/css3%20-%231572B6.svg?&style=for-the-badge&logo=css3&logoColor=white)
![Bootstrap badge](https://img.shields.io/badge/Bootstrap-v4.4.1-563D7C?style=for-the-badge&logo=bootstrap)
![Jquery badge](https://img.shields.io/badge/Jquery-v3.4.1-0769AD?style=for-the-badge&logo=jquery)
![Jekyll badge](https://img.shields.io/badge/jekyll-v.4-CC0000?style=for-the-badge&logo=jekyll)

## Projeto

SEO -> **includes/seo.html**
NAVBAR -> **data/navegacao.yml**

## Página - Inicial

HTML -> **/index.html**
DADOS -> **data/inicial.yml**

## Página - Sobre

HTML -> **includes/pagina_sobre.html**
DADOS -> **data/sobre.yml**
IMAGENS -> **assets/images**

## Página - Mapeamento

HTML -> ****
SELECT -> **data/map_select.yml**
DADOS -> **data/map_[nome do pais].yml**

## Página - Recomendações

HTML -> **includes/pagina_recomendacoes.html**
DADOS (pt) -> **data/recomendacoes_pt.yml**
DADOS (es) -> **data/recomendacoes_es.yml**
DADOS (en)-> **data/recomendacoes_en.yml**
IMAGENS -> **assets/images/recomendacoes**

## Página - Mais referências

HTML -> **includes/pagina_referencias.html**
DADOS (pt) -> **data/referencias_pt.yml**
DADOS (es) -> **data/referencias_es.yml**
DADOS (en)-> **data/referencias_en.yml**

## Página - Contato

HTML -> **includes/pagina_contato.html**
DADOS -> **data/contato.yml**

## Instalação

Ambiente local:

```bash
$ git clone https://gitlab.com/ccsl-ufpa/educacaovigiada-org-br.git
$ cd educacaovigiada-org-br/
$ bundle install
$ bundle exec jekyll serve
```