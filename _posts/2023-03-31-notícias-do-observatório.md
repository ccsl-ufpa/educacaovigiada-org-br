---
title: Notícias do Observatório
date: 2023-03-31T17:16:00.850Z
layout: post
lang: pt
---
Nesse espaço publicaremos notícias e novidades do Observatório Educação Vigiada. Aqui você vai encontrar notícias sobre nossas publicações acadêmicas e técnicas (textos, dados) bem como nossa participação na mídias (entrevistas, webinars, mesas, conferências) e  menções ao projeto na mídia.