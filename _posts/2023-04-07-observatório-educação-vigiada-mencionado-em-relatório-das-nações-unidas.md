---
title: Observatório Educação Vigiada mencionado em relatório das Nações Unidas
date: 2023-04-07T15:50:40.645Z
layout: post
lang: pt
---
Recente documento preparado Relatora Especial, [Koumbou Bola Barry](https://en.wikipedia.org/wiki/Koumbou_Boly_Barry), aponta o observatório Educação Vigiada como uma importante iniciativa da sociedade civil para disseminação de conhecimento sobre privacidade e vigilância na educação.

O [relatório](https://www.ohchr.org/en/documents/thematic-reports/ahrc5032-impact-digitalization-education-right-education) sobre o *impacto da digitalização da educação no direito a educação*, preparado para o Conselho de Direitos Humanos das Nações Unidas, tem como objetivo reforçar a importância de direcionar esforços de digitalização na direção de uma educação pública, de qualidade, para todos.