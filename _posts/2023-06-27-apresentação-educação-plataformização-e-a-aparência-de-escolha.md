---
title: "Apresentação: Educação, plataformização e a aparência de escolha"
date: 2023-06-27T10:46:47.785Z
layout: post
lang: pt
---
No dia 29 de junho, o pesquisador do Observatório Educação Vigiada, Prof. Tel Amiel, fará apresentação sobre plataformização da educação no Centre for Research in Digital Education (Univ. de Edinburgh). O evento será online, em inglês, com [inscrições abertas](https://www.de.ed.ac.uk/event/education-platformization-and-semblance-choice). Abaixo um resumo:

A fala tem por objetivo apresentar os dados coletados pelo Observatório, já disponíveis em nosso site, e também dados novos coletados em 2021 tendo como foco as secretarias de educação do Brasil. Por fim, haverá uma discussão sobre como a plataformização promove um paradoxo instrumentalista: ela apresenta as novas tecnologias (inclusive a IA generativa) como uma *escolha* e uma *mera ferramenta* à nossa disposição, ao mesmo tempo em que diminui sistematicamente a capacidade de ação das instituições dos professores.