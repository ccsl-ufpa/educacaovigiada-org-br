---
title: "Mapeamento da plataformização da educação pública superior: América
  Latina e África"
date: 2024-05-22T18:01:43.775Z
layout: post
lang: pt
---
O [Observatório Educação Vigiada](https://educacaovigiada.org.br/) apresenta o resultado das pesquisas “Plataformização da Educação Pública Superior na África” e "Plataformização da Educação Pública Superior na América Latina". São dados inéditos, frutos do mapeamento das soluções de armazenamento e gerenciamento de e-mails institucionais das Instituições Públicas de Ensino Superior das regiões pesquisadas. Os dados das pesquisas estão publicadas no relatório ["Mapeamento da plataformização da educação pública superior: América Latina e África"](https://zenodo.org/records/11243189). Ao analisar os registros de direcionamento dos domínios de e-mail pesquisados, temos, resumidamente, os seguintes resultados:

Na **África**:

* **1.621** Instituições Públicas de Ensino Superior pesquisadas em 55 países;
* **1.170** domínios válidos de e-mail institucional encontrados;
* **59%** dos domínios válidos direcionam para servidores da Google ou Microsoft;
* **45%** dos domínios válidos estão armazenados em servidores da Google e 14% nos da Microsoft;
* **41%** dos domínios apresentam outras soluções de armazenamento;
* **27%** do total de domínios válidos direcionam para servidores gerenciados pelas próprias instituições;
* Na Tunísia, **74% dos 64** **e-mails institucionais** encontrados são direcionados para servidores da Tunisie Telecom, empresa de telecomunicações de capital misto, majoritariamente estatal;
* Nas Ilhas Seychelles, **64% dos 14 domínios** encontrados são direcionados para servidores estatais, gerenciados pelo Departamento de Informação, Comunicação e Tecnologia do Governo de Seychelles;
* Na Tanzânia, **45% dos domínios válidos** são direcionados para servidores próprios, gerenciados pela Autoridade de Governo Eletrônico (e-Government Authority), ligada à administração pública federal.

Na **América Latina**:

* Foram pesquisadas **666 domínios** de e-mail em 20 países;
* Foram encontrados **646 domínios** válidos de e-mail institucional;
* **76%** dos domínios válidos das Instituições Públicas de Ensino Superior da América Latina direcionam suas mensagens para servidores da Google e da Microsoft;
* **58%** dos domínios válidos estão armazenados na Google e **18%** na Microsoft;
* Somente **24%** dos domínios válidos estão armazenados em outros tipos de solução, majoritariamente servidores próprios e gerenciados pelas instituições;
* Cuba é o único país da região com **100%** dos servidores próprios, seguido por Uruguai, com **85%**.

[Acesse aqui](https://zenodo.org/records/11243189) o relatório completo.