---
title: Workshop Educação Vigiada no FIB 14
date: 2024-03-21T21:11:04.575Z
layout: post
lang: pt
---
O [Observatório Educação Vigiada](https://educacaovigiada.org.br/) estará presente no [Fórum da Internet no Brasil - FIB14](https://forumdainternet.cgi.br/fib14/), que acontecerá em Curitiba/PR entre 21 e 24 de maio de 2024. O FIB é realizado anualmente pelo Comitê Gestor da Internet no Brasil (CGI.br), sendo um importante espaço de debates sobre a rede.

Será ministrado o workshop **A plataformização da educação na América Latina e na África e os desafios da soberania digital no Sul Global**. A discussão apresentará dados sobre a plataformização da educação na América Latina e África, debaterá sobre os riscos desse fenômeno para a educação pública e destacará soluções implementadas em diferentes territórios.

Participarão do encontro Janaina do Rozário Diniz (UEMG/EdVig) e Leonardo Cruz (UFPA/EdVig), representando a Comunidade Científica e Tecnológica; Everton Vasconcelos de Almeida (professor na Secretaria de Estado da Educação de Santa Catarina) representando o setor governamental; Vanda Santana (Sindicato dos Trabalhadores em Educação Pública do Paraná) representando o Terceiro Setor; e Eder Miranda (Unodata) e Frank Karlitschek (Nextcloud) representando o setor empresarial.

Nos vemos lá!