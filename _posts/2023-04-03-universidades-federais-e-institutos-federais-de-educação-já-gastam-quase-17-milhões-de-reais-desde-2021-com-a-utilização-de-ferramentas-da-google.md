---
title: Universidades Federais e Institutos Federais de Educação já gastam quase
  17 milhões de reais desde 2021 com a utilização de ferramentas da Google.
date: 2023-04-03T19:46:25.192Z
layout: post
lang: pt
---
Segundo pesquisa realizada pelo Observatório Educação Vigiada a partir de dados do Diário Oficial da União, as Universidades Federais e Institutos Federais de Educação do Brasil [já gastaram R$16.885.971,19](https://zenodo.org/record/7797426) desde 2021 para a aquisição de licenças de uso das plataformas educacionais Google Workspace for Education. Até agora, ao menos dezenove Universidades Federais e oito Institutos Federais de Educação adquiriram as licenças de uso. Esse número tende a aumentar, pois as limitações presentes no plano gratuíto oferecido às universidades - como a diminuição do espaço de armazenamento e a impossibilidade de gravação de conferências no Google Meet - já apresentam algumas dificuldades à gestão das informações das instituições.

A grande maioria das transações foi realizada através da Rede Nacional de Ensino e Pesquisa (RNP) que, durante a pandemia de COVID-19, ofertou planos gratuitos dessas ferramentas às universidades públicas do Brasil e agora comercializa licenças através do seu serviço NasNuvens. 

A transferência de verbas das Instituições Federais de Ensino Superior às grandes empresas de tecnologia representa mais um entrave em relação à soberania tecnológica do país, impedindo que esse dinheiro seja investido em uma infraestrutura própria e sob controle institucional.