---
title: Tecnologias a serviço de quem?
date: 2023-06-17T19:05:06.870Z
layout: post
lang: pt
---
O que diriam Paulo Freire, Álvaro Vieira Pinto sobre o avanço do capitalismo de vigilância na educação pública? Como esses autores, juntamente com Evgeny Morozov e Sérgio Guimarães, podem contribuir para a reflexão sobre a vigilância na educação? Tais questões impulsionaram a elaboração do artigo *Tecnologias a serviço de quem?* um diálogo entre Álvaro Vieira Pinto, Evgeny Morozov, Paulo Freire e Sérgio Guimarães sobre capitalismo de vigilância na educação.

Por meio de uma pesquisa bibliográfica, o trabalho realiza uma discussão sobre o tema a partir da seleção e da análise de obras dos autores. Constata-se que Pinto, Morozov, Freire e Guimarães defendem o desenvolvimento da consciência crítica sobre as tecnologias, a partir da compreensão dos contextos histórico, econômico e político em que as tecnologias são desenvolvidas. Leia o artigo [aqui](https://periodicos.ufmg.br/index.php/textolivre/article/view/42201).