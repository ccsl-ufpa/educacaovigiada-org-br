---
title: Grupo Público do Educação Vigiada no Matrix e Telegram
date: 2023-05-11T15:05:13.334Z
layout: post
lang: pt
---
O Educação Vigiada mantém um grupo integrado no Matrix e Telegram para discussão pública sobre o projeto e temas relacionados.

O acesso ao grupo no Matrix se dá pela sala [#edvig-comunidade:matrix.org](https://matrix.to/#/%23edvig-comunidade:matrix.org), enquanto no Telegram estamos no [EdVigComunidade](https://t.me/EdVigComunidade).

Como as duas redes estão integradas, mensagens em qualquer dos grupos são replicadas no outro. Apesar disso, recomendamos aos usuários darem preferência ao acesso via Matrix pois pretendemos encerrar o grupo no Telegram em um futuro próximo.
