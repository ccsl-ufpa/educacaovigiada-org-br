https://jekyllrb.com/
- Site do Jekyll

https://educacaovigiada.org.br/
- Educação vigiada

https://getbootstrap.com/docs/4.6/getting-started/introduction/
- Bootstrap 4.6

https://forestry.io/blog/creating-a-multilingual-blog-with-jekyll/
- Internacionalização com jekyll

https://shopify.github.io/liquid/
- Documentação do liquid

https://leafletjs.com/
- gera mapas js

https://www.mapbox.com/
- provider do mapa

https://whereby.com/laai
- Reunião

https://www.latlong.net/
- pegar lat e lng

http://www.yamllint.com/
- check yml data

https://www.chartjs.org/docs/latest/
- gera chart

https://v2_0_0-beta_1--chartjs-plugin-datalabels.netlify.app/
- plugin pra gerar label no chart

https://pixabay.com/pt/
- Free photo stock